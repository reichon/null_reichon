
//import  java.util.*;
import  java.awt.*;
import  java.awt.event.*;
import  javax.swing.*;
import  javax.swing.event.*;

@SuppressWarnings("serial")
public class PSViewer extends JFrame 
	implements ActionListener, ChangeListener {

		public static void main(String[] arg) {
			new PSViewer();
		}
		
		JPanel  textp, viewp;  // 左右のパネル
		JPanel  psview; // postscriptを描画するパネル
		JTextArea  source; // PostScriptの入力エリア
		JButton draw; // 描画ボタン
		JSlider  sizer; // 拡大・縮小スライダー
		JLabel  sizelabel; // %表示のためのラベル
		
		// コンストラクタ
		public PSViewer() {
			super( "PostScript View" );
			setSize( 800, 600 );
			setLayout( new GridLayout( 1, 2 ) );  // 全体は1行2列
			
			// 左側のパネル
			textp = new JPanel();
			textp.setLayout( new BorderLayout() );
			// PostScript入力用テキストエリア
			source = new JTextArea( 20, 15 ); // 20行15文字
			source.setFont( new Font( "SansSerif", Font.PLAIN, 24 ) );
			textp.add( "Center", new JScrollPane( source ) ); // スクロール可能なように
			// 描画ボタン
			JPanel buttonp = new JPanel();
			draw = new JButton( "描画" );
			draw.addActionListener( this );
			buttonp.add( draw );
			textp.add( "South", buttonp );
			add( textp );
			
			// 右側のパネル
			viewp = new JPanel();
			viewp.setLayout(new BorderLayout() );
			// 描画用のパネル
			psview = new JPanel();
			psview.setSize( 800, 400 );  // 最大描画領域を決める
			viewp.add( "Center", new JScrollPane( psview ) ); // スクロールが可能なように
			// 拡大縮小のスライダーとラベル
			JPanel  slidep = new JPanel();
			sizelabel = new JLabel( "拡大・縮小： 100%");
			slidep.add( sizelabel );
			sizer = new JSlider( 10, 1000 );
			sizer.setValue( 100 );
			sizer.addChangeListener( this );
			slidep.add( sizer );
			viewp.add( "South", slidep );
			add( viewp );
			
			// ウィンドウを出現させる
			setDefaultCloseOperation(EXIT_ON_CLOSE);
			setVisible( true );
		}

		// 今はパックマンを表示しているだけですが、これを
		// PostScriptを表示するような形に変えて下さい
		public void paint(Graphics g) {
			super.paint( g );  // Swingの場合はこれが必要です
			Graphics pg = psview.getGraphics();
			double scale = sizer.getValue()  / 100.0;
			pg.setColor( Color.yellow );
			int cx = viewp.getWidth() / 2 ;
			int cy = viewp.getHeight() / 2;
			int size = (int)(((cx < cy) ? cx * 3 / 4 : cy * 3 / 4) * scale) ;
			pg.fillArc( cx-size, cy-size, size*2, size*2, 45, 270 );
		}

		// 描画ボタンが押されたときに呼び出される
		@Override
		public void actionPerformed(ActionEvent e) {
			// ここでsource.getText()として、テキストエリアに
			// 入力されたPostScriptの文字列を得てから、
			// 一行ごとに解釈していきます。
			repaint();
		}

		// スライダーが動かされたときに呼び出される
		@Override
		public void stateChanged(ChangeEvent e) {
			sizelabel.setText( String.format( "拡大・縮小： %d%%", sizer.getValue()) );
			repaint();
		}
	}

