import  java.applet.*;
import  java.awt.*;

@SuppressWarnings("serial")
public class practice0801 extends Applet {

	public void paint(Graphics g) {
		int  centerx = getWidth() / 2;  // 中心のx座標
		int  centery = getHeight() / 2;  // 中心のy座標
		//座標系を描く
		g.setColor(Color.blue);
		g.drawLine(0, centery, getWidth(), centery);//水平線
		g.drawLine(centerx, 0, centerx, getHeight());//垂直線
		
		//リサージュ図形を描く
		g.setColor(Color.red);
		int  n = 10000;  // 何本の折れ線で曲線を近似するか
		double  t = 0;  // 媒介変数
		double  delta = 2 * Math.PI / n;  // 媒介変数の１回の差分
		// 半径はウィンドウの幅と高さのうち、小さい方の半分の８割にする
		double  radius = (centerx < centery) ? centerx *0.8 : centery *0.8;
		int coscyc = 5; //cosの周期倍率
		int sincyc = 6; //sinの周期倍率
		
		int  lastx = centerx + (int)( radius * Math.cos( t*coscyc ) );
		int  lasty = centery - (int)( radius * Math.sin( t*sincyc ) );
		for ( int i=1; i <= n; i++ ) {
			t += delta;  // 差分を足す
			int  x = centerx + (int)( radius * Math.cos( t*coscyc ) );
			int  y = centery - (int)( radius * Math.sin( t*sincyc ) );
			g.drawLine( lastx, lasty, x, y );
			lastx = x;
			lasty = y;
		}
	}
}