//import java.util.*;

public class practice1202 {
	//その月の日数がdays
	static int days[] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30 ,31};
	//0...日曜始まり ~ 6...土曜始まり
	static int sday[] = {4, 0, 0, 3, 5, 1, 3, 6, 2, 4, 0, 2};
	public static void main(String[] args) {
		for(int m = 0; m < days.length; m++){
			System.out.println((m+1)+"月");
			System.out.println("Sun Mon Tue Wed The Fri Sat");
			printMonth(days[m], sday[m]);
			System.out.println();
		}
	}
	
	
	static void printMonth(int days, int sday){
		for(int pad = 0; pad < sday; pad++){
			System.out.print("--- ");
		}
		for(int day = 0; day < days; day++){
			System.out.printf("%3d ", (day+1));
			if((day + sday) %7 == 6){
				System.out.println();
			}
		}
		System.out.println();
	}

}
