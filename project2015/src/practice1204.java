import java.applet.*;
import java.awt.*;

@SuppressWarnings("serial")
public class practice1204 extends Applet{
	public void paint(Graphics g){
		int map[][] = new int[80][80];
		
		//mapの値の初期化
		for(int i = 0; i < map.length; i++){
			for(int j = 0; j < map[i].length; j++){
				//左上の端の点
				if(i == 0 & j == 0){
					int height = (int)(Math.random() * 7);
					map[i][j] = height;
				}
				//左端の点は、1つ上の点を参照する
				else if(i == 0){
					int height = randomHeight(map[i][j-1]);
					map[i][j] = height;
				}
				//上端の単は、1つ左の点を参照する
				else if(j == 0){
					int height = randomHeight(map[i-1][j]);
					map[i][j] = height;
				}
				/*else if(map[i][j-1] == map[i-1][j]){
					int height = randomHeight(map[i-1][j]);
					map[i][j] = height;
				}*/
				//それ以外の点の場合は、
				else{
					int up = map[i][j-1];
					int left = map[i-1][j];
					//1つ上と1つ左が同じ値だったら、-1〜+1の間で
					if(up == left){
						int height = randomHeight(up);
						map[i][j] = height;
					}
					//1つ上と1つ左の値が2以上離れていたら平均を取る
					else if(Math.abs(up - left) >= 2){
						int height = (map[i][j-1] + map[i-1][j])/2;
						map[i][j] = height;
					}
					//小さいほうの値 +0か+1にする
					else if(up > left){
						map[i][j] = left + (int)(Math.random()*2);
					}else{
						map[i][j] = up + (int)(Math.random()*2);
						
					}
					
				}
			}
		}
		
		//mapの値の表示
		for(int i = 0; i < map.length; i++){
			for(int j = 0; j < map[i].length; j++){
				if(map[i][j] == 0){g.setColor(Color.magenta);}
				if(map[i][j] == 1){g.setColor(Color.blue);}
				if(map[i][j] == 2){g.setColor(Color.cyan);}
				if(map[i][j] == 3){g.setColor(Color.green);}
				if(map[i][j] == 4){g.setColor(Color.yellow);}
				if(map[i][j] == 5){g.setColor(Color.orange);}
				if(map[i][j] == 6){g.setColor(Color.red);}
				g.fillRect(i*10, j*10, 10, 10);
			}
		}
	}
	/**
	 * @return refかref+1かref-1 (ただし0〜6の間で)
	 */
	int randomHeight(int ref){
		int value = ref + (int)(Math.random() * 3) - 1;
		if(value < 0){value = 0;}
		if(value < 6){value = 6;}
		return value;
	}
}
