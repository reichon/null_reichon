import sfc.util.*;
public class practice0501 {
	public static void main(String[] arg){
		int year = Terminal.inputInteger("西暦を入力");
		String nengo = "";
		if(year < 1864){
			nengo = year + "年は慶應以前";
		}else if(year < 1868){
			nengo = year + "年は慶應" + (year - 1863) + "年"; 
		}else if(year < 1912){
			nengo = year + "年は明治" + (year - 1867) + "年";
		}else if(year < 1925){
			nengo = year + "年は大正" + (year - 1911) + "年";
		}else if(year < 1989){
			nengo = year + "年は昭和" + (year - 1924) + "年";
		}else{
			nengo = year + "年は平成" + (year - 1988) + "年";
		}
		System.out.println(nengo);
	}
}
