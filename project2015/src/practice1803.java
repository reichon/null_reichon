import java.applet.*;
import java.awt.*;
import java.awt.geom.*;
import java.util.*;

class Command{
	enum Type {newpath, moveto, lineto, curveto, arc, translate, rotate, stroke}; //closepath, curvetoを完成させる
	Type Command;
	double x1, x2, x3;
	double y1, y2, y3;
	double r;
	double st, ed;
	
	//コンストラクタ
	public Command(Type com){
		Command = com;
	}
	public Command(Type com, double rad){
		Command = com;
		r = rad;
	}
	public Command(Type com, double x, double y){
		Command = com;
		x1 = x;
		y1 = y;
	}
	public Command(Type com, double x, double y, double radius, double sta, double eda){
		Command = com; x1 = x; y1 = y; r = radius; st = sta; ed = eda;
	}
}

@SuppressWarnings("serial")
public class practice1803 extends Applet{
	
	ArrayList<Command> Commandlist;
	
	public void init(){
		Command array [] = {
				new Command(Command.Type.newpath),
				new Command(Command.Type.moveto, 100, 100),
				new Command(Command.Type.lineto, 200, 100),
				new Command(Command.Type.lineto, 200, 200),
				new Command(Command.Type.arc, 200, 400, 50, 45, 315),
				new Command(Command.Type.translate, 100, 0),
				new Command(Command.Type.rotate, 20),
				new Command(Command.Type.stroke)
				};
		Commandlist = new ArrayList<Command>();
		for(Command com : array){
			Commandlist.add(com);
		}
	}
	
	public void paint(Graphics g){
		drawPostScript(g);
	}
	public void drawPostScript(Graphics g){
		Graphics2D g2 = (Graphics2D)g;
		g2.translate(0, getHeight());
		g2.scale(1, -1);
		GeneralPath gp = new GeneralPath();
		ArrayList<Arc2D.Double> arclist = new ArrayList<Arc2D.Double>();
		
		for(Command com : Commandlist){
			if(com.Command == Command.Type.newpath){
				gp = new GeneralPath();
			}else if(com.Command == Command.Type.moveto){
				gp.moveTo(com.x1, com.y1);
			}else if(com.Command == Command.Type.lineto){
				gp.lineTo(com.x1, com.y1);
			}else if(com.Command == Command.Type.stroke){
				g2.draw(gp);
				for(Arc2D.Double arc: arclist){
					g2.draw(arc);
				}
			}else if(com.Command == Command.Type.translate){
				g2.translate(com.x1, com.y1);
			}else if(com.Command == Command.Type.rotate){
				g2.rotate(Math.toRadians(com.r));
			}else if(com.Command == Command.Type.arc){
				Arc2D.Double a = new Arc2D.Double(
						com.x1 - com.r, com.y1 - com.r, com.r * 2, com.r * 2,
						com.st, com.ed - com.st, Arc2D.OPEN);
				arclist.add(a);
			}
		}
	}

}
