import java.applet.*;
import java.awt.*;
import java.awt.event.*;


@SuppressWarnings("serial")
public class practice1703 extends Applet implements KeyListener{
	
	enum Dir{North, South, West, East};
	Dir pdir = Dir.East;
	int px = 100, py = 100;
	
	public void init(){
		setBackground(Color.blue);
		addKeyListener(this);
	}
	
	public void paint(Graphics g){
		g.setColor(Color.yellow);
		if(pdir == Dir.East){g.fillArc(px, py, 100, 100, 45, 270);}
		if(pdir == Dir.West){g.fillArc(px, py, 100, 100, 225, 270);}
		if(pdir == Dir.North){g.fillArc(px, py, 100, 100, 135, 270);}
		if(pdir == Dir.South){g.fillArc(px, py, 100, 100, 315, 270);}
	}

	public void keyTyped(KeyEvent e) {}

	public void keyPressed(KeyEvent e) {
		int code = e.getKeyCode();
		int diff = 20;
		if(e.isShiftDown()){diff += 20;}
		
		if(code == 37){pdir = Dir.West; px -= diff;}
		if(code == 39){pdir = Dir.East; px += diff;}
		if(code == 38){pdir = Dir.North; py -= diff;}
		if(code == 40){pdir = Dir.South; py += diff;}
		if(px > getWidth()){px = -90;}
		if(px < -90){px = getWidth() - 10;}
		if(py > getHeight()){py = -90;}
		if(py < -90){py = getHeight()-10;}
		
		repaint();
	}

	public void keyReleased(KeyEvent e) {}

}
