
public class practice0502 {

	public static void main(String[] args) {
		int n = 1;
		while(n <= 10){
			String ordinal = "";
			switch(n){
			case 1: ordinal = "First"; break;
			case 2: ordinal = "Second"; break;
			case 3: ordinal = "Third"; break;
			default: ordinal = n + "th"; //break;
			}
			System.out.println(ordinal);
			n++;
		}
	}

}
