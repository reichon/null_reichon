
public class practice0903 {
	public static void main(String[] arg){
		Integer x = 10;
		Integer y = new Integer(10);
		//別のオブジェクト(識別子等価性)
		if(x == y){
			System.out.println("x is equal to y");
		}else{
			System.out.println("x is not equal to y");
		}
		//整数値と比較すると値等価性になる
		if(y == 10){
			System.out.println("y is equal to 10");
		}else{
			System.out.println("y is not equal to 10");
		}
		//定数値でもオブジェクトを作って飛白すると識別子等価性になる
		if(y == new Integer(10)){
			System.out.println("y is equal to integer 10");
		}else{
			System.out.println("y is not equal to integer 10");
		}
		//値等価性にしたい場合は、equalsを使う
		if(x.equals(y)){
			System.out.println("x is equivalent to y");
		}else{
			System.out.println("x is not equivalent to y");
		}
	}
}
