import java.util.*;
import java.applet.*;
import java.awt.*;
import java.awt.event.*;

public class practice1502 extends Applet implements ActionListener {

	TextField inputf; //入力フィールド
	TextArea resultf; //出力フィールド
	
	Button b;
	
	String specials [][] = {{"ox", "oxen"}, {"axis", "axes"},
			{"man", "men"}, {"woman", "women"}};
	Hashtable<String,String> special;
	
	public void init() {
		//特殊な複数形のハッシュテーブルの初期化
		special = new Hashtable<String, String>();
		for(int i=0; i<specials.length; i++){
			special.put(specials[i][0], specials[i][1]);
		}
		inputf = new TextField(30); //30文字くらいはいる
		inputf.addActionListener(this); //return/enterキーを押すとこのアプレットが対処
		add(inputf); //画面上にコンポーネントを配置
		b = new Button("複数形を求める");
		b.addActionListener(this);
		add(b);
		resultf = new TextArea(10, 30); //20行で30文字
		add(resultf);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		String single = inputf.getText();
		String plural = special.get(single);
		if(plural != null){
			resultf.append(plural + "\n");
		}else if(single.endsWith("s") || single.endsWith("x") ||
					single.endsWith("sh") || single.endsWith("ch")){
			resultf.append(single + "es\n");
		}else if(single.endsWith("y")){
			resultf.append(single.substring(0, single.length()-1) + "ies\n");
		}else{
			resultf.append(single + "s\n");
		}
	}


}
