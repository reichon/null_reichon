import java.util.*;

public class practice1003 {
	
	Scanner scanner;

	public static void main(String[] args) {
		practice1003 self = new practice1003();
		int n = self.inputValue("整数を入力 : ");
		int m = self.inputValue("別の整数を入力 : ");
		System.out.printf("入力された数は%dと%d\n", n, m);
		System.out.printf("それぞれの数の二乗は%dと%d\n", 
				self.square(n), self.square(m));
		System.out.printf("2つの数の差は、%d\n",
				self.greater(n, m) - self.lesser(n, m));
	}
	
	/**
	 * コンソールにプロンプトメッセージを表示して、整数の入力をもらうメソッド
	 * @param message プロンプトのメッセージ
	 * @return 入力値
	 */
	int inputValue(String message){
		System.out.print(message);
		//System.inの入力ストリームから、スキャナで整数に直す
		scanner = new Scanner(System.in);
		return scanner.nextInt();
	}
	
	/**
	 * squareは、パラメータの二乗を返す
	 * @param x パラメータ
	 * @return 二乗の値
	 */
	int square(int x){return x * x;}
	long square(long x){return x * x;}
	float square(float x){return x * x;}
	double square(double x){return x * x;}
	
	int greater(int x, int y){return (x > y) ? x : y;}
	double greater(double x, double y){return (x > y) ? x : y;}
	int greatest(int x, int y, int z){
		return (x >= y && y >= z) ? x : (y >= z) ? y : z;
	}
	
	int lesser(int x, int y){return (x < y) ? x : y;}
	double lesser(double x, double y){return (x < y) ? x : y;}
	
	int least(int x, int y, int z){
		return (x <= y && y <= z) ? z : (x <= y) ? y : x;
	}
}
