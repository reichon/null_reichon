
import  java.util.*;
import  sfc.util.*;

public class tlanslation71344192 {

	// 足りない単語は補充して下さい
	static String startup [][] = { 
		{ "cat", "noun:猫"}, { "dog", "noun:犬" },
		{ "runs", "verb:走る"}, { "jumps", "verb:ジャンプする" },
		{ "at", "prep:で" }, { "garden", "noun:庭" }, 
		{ "a", "article:一匹の" }, { "the", "article:その" },
		{ "red", "adj:赤い" }, { "white", "adj:白い" },
		{"with","prep:と一緒に"}, { "in","prep:の中で"}
	};
	
	public static void main(String[] arg) {
		// startupの配列から、ハッシュテーブルを作る
		Hashtable<String,String> dict = new Hashtable<String,String>();
		for ( int i=0; i < startup.length; i++ ) {
			dict.put( startup[ i ][ 0 ], startup[ i ][ 1 ] );
		}
		// コマンドを打ち込んでもらって、追加・削除・一覧表示
		String command;
		while ( true ) {
			command = Terminal.inputString( "コマンド入力：" );
			if ( command.equals( "quit" ) ) { break; }
			// 一覧表示
			if ( command.equals( "list" ) ) {
				Enumeration<String> keys = dict.keys();
				while( keys.hasMoreElements() ) {
					String key = keys.nextElement();
					System.out.printf( "%s: %s\n", key, dict.get( key ) );
				}
			}
			// 追加
			if ( command.equals( "append" ) ) {
				String english = Terminal.inputString( "英単語入力：");
				String japanese = Terminal.inputString( "和訳入力：" );
				String kind = Terminal.inputString( "品詞入力：");
				if ( dict.get( english ) != null ) {
					System.out.println( "その英単語「"+ english +"」は既に入力済みです。" );
				} else {
					dict.put( english, kind+":"+ japanese );
				}
			}
			// 削除
			if ( command.equals( "remove") ) {
				String english = Terminal.inputString( "削除する英単語入力：");
				if ( dict.get( english ) == null ) {
					System.out.println( "その英単語「"+ english +"」はありません" );
				} else {
					dict.remove( english );
				}
			}
			// 逐語変換
			if(command.equals("translate")){
				String text = Terminal.readLine( "英文入力：");
					if( text.equals("")){break;}
				    //単語に分解
				    String words [] = text.split( " " );
				    String result ="";
				    String verbresult = "";//動詞を最後に持ってくる
				    String prepresult = "";//前置詞を後ろに持ってくるため
				    for( int i=0; i < words.length; i++ ){
				     //該当する英単語があるかどうか
				    	String trans = dict.get( words[ i ] );
				     //ない場合は、そのまま英単語を使う
				    	if ( trans == null ){
				    		result += words[ i ];
				    	}else {
				    		String wayaku = trans.substring(trans.indexOf(":") + 1);//コロン以降を表示
				    		String hinshi = trans.substring( 0, trans.indexOf(":"));
				    		//もし品詞が動詞だった場合
				    		if( hinshi.equals("verb") ){
				    			result += "が";
				    			verbresult += wayaku;
				    		}
				    		//もし品詞が前置詞だった場合
				    		else if ( hinshi.equals("prep")){
				    			result += prepresult;
				    			prepresult = wayaku;
				    		}else{
				    			result += wayaku;
				    		}
				    	}
				    }
				    //最後に動詞と前置詞の訳を加えて表示する
				    System.out.println( result + prepresult + verbresult );
			}
		}
	}
}
			
			
//			if ( command.equals( "translate" ) ) {
//				String text = Terminal.readLine( "英文入力：" );
//				String words [] = text.split( " " );
//				String result ="";
//				String verbresult = "";  // 動詞を最後に持ってくるため
//				String prepresult=""; //前置詞を後ろに持ってくるため				
//				for ( int i=0; i < words.length; i++ ) {
//					//該当する英単語があるかどうか
//					String trans = dict.get( words[ i ] );
//					//ない場合はそのまま英単語を使う
//					if ( trans == null ) {
//						result += words[ i ];
//					} 
//					//ある場合は和訳と品詞を取ってくる
//					else {
//						String wayaku = trans.substring( trans.indexOf(":")+1 );
//						String hinshi = trans.substring( 0, trans.indexOf( ":") );
//						//もし品詞が動詞だった場合
//						if ( hinshi.equals( "verb")  ) {
//							verbresult = wayaku;
//						}
//						//もし品詞が前置詞だった場合 
//						else if(hinshi.equals("prep")){
//							prepresult += wayaku;
//						}
//						else{
//							result += wayaku;
//						}
//					}
//				}
//				//最後に動詞の訳を加えて表示する
//				System.out.println( result + prepresult + verbresult );
//			}
//		}
//	}
//}