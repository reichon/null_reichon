
import  java.util.*;
import  java.awt.*;
import  java.awt.event.*;
import  java.awt.geom.*;

import  javax.swing.*;
import  javax.swing.event.*;

// paintメソッドへ受け渡し用のコマンド表現用のクラス
class PSCommand {
	// 描画コマンドの列挙型
	enum Type { newpath, moveto, lineto, 
		curveto, arc, closepath, translate, rotate, scale,
		stroke, fill, rgbcolor };
		Type command;  // 描画コマンド
		double x1, x2, x3; // x座標関係の変数
		double y1, y2, y3; // y座標関係の変数
		double r;  // 半径、もしくは回転角度として用いる
		double st, ed; // 開始角・終了角度
		float red, green, blue; //rgb

		// コンストラクタ（コマンドだけ用）
		public PSCommand( Type com ) {  command = com; }

		// コンストラクタ（コマンド パラメータ１つ用）
		public PSCommand( Type com, double rad ) {
			command = com; r = rad;
		}

		// コンストラクタ（コマンド パラメータ２つ用）
		public PSCommand( Type com, double x, double y ) {
			command = com; x1 = x; y1 = y;
		}
		
		// コンストラクタ（コマンド　パラメータ３つ用）
		public PSCommand( Type com, float r, float g, float b ) {
			command = com; red = r; green = g; blue = b;
		}

		// コンストラクタ（コマンド　パラメータ５つ用）
		public PSCommand( Type com, double x, double y, double rad, double sta, double eda ) {
			command = com; x1 = x; y1 = y;
			r = rad; st = sta; ed = eda;
		}
		
		// コンストラクタ（コマンド　パラメータ７つ用）
		public PSCommand( Type com, double a, double b, double c, double d, double e, double f ) {
			command = com; x1 = a; y1 = b;
			x2 = c; y2 = d; x3 = e; y3 = f;
		}
}

// PostScriptを表示するためのパネル（スマートコンポーネント）
@SuppressWarnings("serial")
class PSPanel extends JPanel {

	ArrayList<PSCommand> commandlist; // コマンドリスト
	double zoom = 1.0; // 拡大・縮小率

	// 指定されたlistをcommandlistに登録する
	public void setCommandList( ArrayList<PSCommand> list ) {
		commandlist = list;
	}

	// 拡大・縮小率を設定する
	public void setZoom( double z ) { zoom = z; }

	// 描画メソッド
	public void paint(Graphics g) {
		super.paint( g );  // Swingの場合はこれが必要です
		drawPostScript( g );
	}

	// 指定された描画領域にcommandlistにあるコマンドの内容に従って
	// 描画を行なうメソッド
	public void drawPostScript( Graphics g ) {
		// PostScriptにあわせて、パネルの左下を0, 0の位置にし、上方向にy座標を向ける
		Graphics2D g2 = (Graphics2D)g;
		g2.translate( 0, 800 );
		g2.scale( 1, -1 );

		// スライダの大きさに併せて更に拡大・縮小を行なう
		g2.scale( zoom, zoom );

		// GeneralPathとArc2Dを使って描画を行なう
		GeneralPath gp = new GeneralPath();
		ArrayList<Arc2D.Double> arclist = new ArrayList<Arc2D.Double>();

		// コマンドリストの各要素ごとに描画を実行していく
		for ( PSCommand com: commandlist ) {
			if ( com.command == PSCommand.Type.newpath ) {
				gp = new GeneralPath();
			} else if ( com.command == PSCommand.Type.moveto ) {
				gp.moveTo( com.x1, com.y1 );
			} else if ( com.command == PSCommand.Type.lineto ) {
				gp.lineTo( com.x1, com.y1 );
			} else if ( com.command == PSCommand.Type.curveto ) {
				gp.curveTo( com.x1, com.y1, com.x2, com.y2, com.x3, com.y3);	
			} else if ( com.command == PSCommand.Type.stroke ) {
				g2.draw( gp );
				for ( Arc2D.Double arc: arclist ) { g2.draw( arc ); } // strokeがされたらarclistも全部描画する
				arclist.clear(); // 描いた後は破棄する
			} else if ( com.command == PSCommand.Type.translate ) {
				g2.translate( com.x1, com.y1 );
			} else if ( com.command == PSCommand.Type.rotate ) {
				g2.rotate( Math.toRadians( com.r ) );
			} else if ( com.command == PSCommand.Type.arc ) {
				Arc2D.Double a = new Arc2D.Double( com.x1 - com.r, com.y1 - com.r,
						com.r * 2, com.r *2, com.st, com.ed - com.st, Arc2D.CHORD );
				arclist.add( a );
			} else if ( com.command == PSCommand.Type.scale ) {
				g2.scale( com.x1, com.y1 );
			} else if ( com.command == PSCommand.Type.closepath ) {
				gp.closePath();
			} else if ( com.command == PSCommand.Type.fill ) {
				g2.fill( gp );
			} else if ( com.command == PSCommand.Type.rgbcolor ) {
				g2.setColor( new Color( com.red, com.green, com.blue ) );
			}	
		}		
	}
}

@SuppressWarnings("serial")
public class PSViewer71344192 extends JFrame implements ActionListener, ChangeListener {

	public static void main(String[] arg) {
		new PSViewer71344192();
	}

	JPanel  textp, viewp;  // 左右のパネル
	PSPanel  psview; // PostScriptを描画するパネル
	JTextArea  source; // PostScriptの入力エリア
	JButton draw; // 描画ボタン
	JSlider  sizer; // 拡大・縮小スライダー
	JLabel  sizelabel; // %表示のためのラベル
	ArrayList<PSCommand> commandlist; // コマンドリスト

	// コンストラクタ
	public PSViewer71344192() {
		super( "PostScript View" );
		// 全体の設定
		setSize( 800, 600 );
		setLayout( new GridLayout( 1, 2 ) );  // 全体は1行2列

		// コマンドリストの初期化
		commandlist = new ArrayList<PSCommand>();

		// 左側のパネル
		textp = new JPanel();
		textp.setLayout( new BorderLayout() );
		// PostScript入力用テキストエリア
		source = new JTextArea( 20, 15 ); // 20行15文字
		source.setFont( new Font( "SansSerif", Font.PLAIN, 24 ) );
		textp.add( "Center", new JScrollPane( source ) ); // スクロール可能なように
		// 描画ボタン
		JPanel buttonp = new JPanel();
		draw = new JButton( "描画" );
		draw.addActionListener( this );
		buttonp.add( draw );
		textp.add( "South", buttonp );
		add( textp );

		// 右側のパネル
		viewp = new JPanel();
		viewp.setLayout(new BorderLayout() );
		// 描画用のパネル
		psview = new PSPanel();
		psview.setPreferredSize( new Dimension( 800, 800 ) );  // 描画領域のサイズを決める
		psview.setCommandList( commandlist );
		JScrollPane scroll = new JScrollPane(); // スクロールが可能なように
		scroll.setViewportView( psview );
		viewp.add( "Center", scroll ); 
		// 拡大縮小のスライダーとラベル
		JPanel  slidep = new JPanel();
		sizelabel = new JLabel( "拡大・縮小： 100%");
		slidep.add( sizelabel );
		sizer = new JSlider( 10, 1000 );
		sizer.setValue( 100 );
		sizer.addChangeListener( this );
		slidep.add( sizer );
		viewp.add( "South", slidep );
		add( viewp );

		// ウィンドウを出現させる
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setVisible( true );
	}

	// 描画ボタンが押されたときに呼び出される
	@Override
	public void actionPerformed(ActionEvent e) {
		commandlist.clear();  // 前に登録したコマンドリストを破棄する
		String text = source.getText(); // テキストエリアに入力されたPostScriptの文字列を得る
		String lines [] = text.split( "\n" ); // 改行コードで１行ずつに分ける

		// 一行ごとに解釈していく
		for ( String line: lines) {
			line = line.trim(); // 余分な前後の空白の削除
			if ( line.equals("newpath") ) {
				commandlist.add( new PSCommand( PSCommand.Type.newpath) );
			} else if ( line.equals("stroke") ) {
				commandlist.add( new PSCommand( PSCommand.Type.stroke) );
			} else if ( line.endsWith("moveto") ) {
				String word [] = line.split(" ");
				double x = Double.parseDouble( word[ 0 ] );
				double y = Double.parseDouble( word[ 1 ] );
				commandlist.add( new PSCommand( PSCommand.Type.moveto, x, y ) );
			} else if ( line.endsWith("lineto") ) {
				String word [] = line.split(" ");
				double x = Double.parseDouble( word[ 0 ] );
				double y = Double.parseDouble( word[ 1 ] );
				commandlist.add( new PSCommand( PSCommand.Type.lineto, x, y ) );
			} else if ( line.endsWith("arc") ) {
				String word [] = line.split(" ");
				double x = Double.parseDouble( word[ 0 ] );
				double y = Double.parseDouble( word[ 1 ] );
				double rad = Double.parseDouble( word[ 2 ] );
				double sta = Double.parseDouble( word[ 3 ] );
				double eda = Double.parseDouble( word[ 4 ] );
				commandlist.add( new PSCommand( PSCommand.Type.arc, x, y, rad, sta, eda) );
			} else if ( line.endsWith("translate") ) {
				String word [] = line.split(" ");
				double x = Double.parseDouble( word[ 0 ] );
				double y = Double.parseDouble( word[ 1 ] );
				commandlist.add( new PSCommand( PSCommand.Type.translate, x, y ) );
			} else if ( line.endsWith("rotate") ) {
				String word [] = line.split(" ");
				double rad = Double.parseDouble( word[ 0 ] );
				commandlist.add( new PSCommand( PSCommand.Type.rotate, Math.toRadians( rad ) ) );
			} else if ( line.endsWith("curveto") ) {
				String word [] = line.split(" ");
				double x1 = Double.parseDouble( word[ 0 ] );
				double y1 = Double.parseDouble( word[ 1 ] );
				double x2 = Double.parseDouble( word[ 2 ] );
				double y2 = Double.parseDouble( word[ 3 ] );
				double x3 = Double.parseDouble( word[ 4 ] );
				double y3 = Double.parseDouble( word[ 5 ] );
				commandlist.add( new PSCommand( PSCommand.Type.curveto, x1, y1, x2, y2, x3, y3 ) );
			} else if ( line.endsWith("closepath") ) {
				commandlist.add( new PSCommand( PSCommand.Type.closepath) );
			} else if ( line.endsWith("scale") ) {
				String word [] = line.split(" ");
				double x = Double.parseDouble( word[ 0 ] );
				double y = Double.parseDouble( word[ 1 ] );
				commandlist.add( new PSCommand( PSCommand.Type.scale, x, y ) );
			} else if ( line.endsWith("fill") ) {
				commandlist.add( new PSCommand( PSCommand.Type.fill) );
			} else if ( line.endsWith("rgbcolor") ) {
				String word [] = line.split(" ");
				float r = Float.parseFloat( word[ 0 ] );
				float g = Float.parseFloat( word[ 1 ] );
				float b = Float.parseFloat( word[ 2 ] );
				commandlist.add( new PSCommand( PSCommand.Type.rgbcolor, r, g, b ) );
			}	
		}
		repaint();
	}

	// スライダーが動かされたときに呼び出される
	@Override
	public void stateChanged(ChangeEvent e) {
		sizelabel.setText( String.format( "拡大・縮小： %d%%", sizer.getValue()) );
		psview.setZoom( sizer.getValue() / 100.0 );
		repaint();
	}
}