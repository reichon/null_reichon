import java.awt.*;
import java.applet.*;

@SuppressWarnings("serial")
public class practice0504 extends Applet {

	public void paint(Graphics g) {
		setBackground(Color.green);
		int day = 1;
		while(day <= 31){
			if(day % 7 == 0){g.setColor(Color.red);}
			else if(day % 7 == 6){g.setColor(Color.blue);}
			else{g.setColor(Color.black);}
			g.drawString(""+day, day % 7 * 40 + 30, day / 7 * 30 + 50);
			day++;
		}
	}

}
