import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;
import java.applet.*;

@SuppressWarnings("serial")
public class practice1802 extends Applet{
	
	public void paint(Graphics g){
		Graphics2D g2 = (Graphics2D)g;
		AffineTransform original = g2.getTransform();
		g2.translate(getWidth()/2, getHeight()/2);
		g2.scale(1.5, -1.5);
		g2.setColor(Color.magenta);
		GeneralPath e = createEiffel();
		for(int n = 0; n < 6; n++){
			g2.translate(0, 20);
			g2.draw(e);
			g2.rotate(Math.toRadians(60));
		}
		g2.setTransform(original);
	}

	GeneralPath createEiffel( ) {
		GeneralPath	eiffel = new GeneralPath( );
		eiffel.moveTo( -10, 0 );
		eiffel.lineTo( 10, 0 );
		eiffel.quadTo( 20, 100, 50, 150 );
		eiffel.lineTo( 20, 150 );
		eiffel.curveTo( 20, 120, -20, 120, -20, 150 );
		eiffel.lineTo( -50, 150 );
		eiffel.quadTo( -20, 100, -10, 0 );
		return eiffel;
	}
}
