import java.util.*;
import java.applet.*;
import java.awt.*;
import java.awt.event.*;

//英単語の辞書の項目を表すクラス
class Word{
	String title; //英単語のタイトル
	enum Parts{Noun, Verb, Adj, Prep, Article, Conf};
	Parts part; //単語の品詞
	String wayaku; //英単語の和訳
	
	//コンストラクタ
	public Word(String t, Word.Parts p, String w){
		title = t; part = p; wayaku = w;
	}
}

@SuppressWarnings("serial")
public class practice1503 extends Applet implements ActionListener{

	Hashtable<String, Word>dict;
	Hashtable<String, String>dict2;
	TextField english, japanese;
	
	Word[] startup = {new Word("cat", Word.Parts.Noun, "猫"),
			new Word("dog", Word.Parts.Noun, "犬"),
			new Word("ball", Word.Parts.Noun, "ボール"),
			new Word("box", Word.Parts.Noun, "箱"),
			new Word("chair", Word.Parts.Noun, "椅子"),
			new Word("floor", Word.Parts.Noun, "床"),
			new Word("garden", Word.Parts.Noun, "庭"),
			new Word("rabbit", Word.Parts.Noun, "うさぎ"),
			new Word("room", Word.Parts.Noun, "部屋"),
			new Word("sofa", Word.Parts.Noun, "ソファ"),
			new Word("jumps", Word.Parts.Verb, "ジャンプする"),
			new Word("runs", Word.Parts.Verb, "走る"),
			new Word("gets", Word.Parts.Verb, "とる"),
			new Word("looks", Word.Parts.Verb, "見る"),
			new Word("moves", Word.Parts.Verb, "動く"),
			new Word("plays", Word.Parts.Verb, "遊ぶ"),
			new Word("sleeps", Word.Parts.Verb, "寝る"),
			new Word("walks", Word.Parts.Verb, "歩く"),
			new Word("is", Word.Parts.Verb, "は"),
			new Word("big", Word.Parts.Adj, "大きい"),
			new Word("black", Word.Parts.Adj, "黒い"),
			new Word("blue", Word.Parts.Adj, "青い"),
			new Word("cold", Word.Parts.Adj, "冷たい"),
			new Word("green", Word.Parts.Adj, "緑の"),
			new Word("heavy", Word.Parts.Adj, "重い"),
			new Word("hot", Word.Parts.Adj, "ア↑ツゥイ↓"),
			new Word("large", Word.Parts.Adj, "広い"),
			new Word("light", Word.Parts.Adj, "軽い"),
			new Word("small", Word.Parts.Adj, "小さい"),
			new Word("orange", Word.Parts.Adj, "オレンジ色の"),
			new Word("pink", Word.Parts.Adj, "ピンク色の"),
			new Word("red", Word.Parts.Adj, "赤い"),
			new Word("yellow", Word.Parts.Adj, "黄色い"),
			new Word("white", Word.Parts.Adj, "白い"),
			new Word("and", Word.Parts.Conf, "そして"),
			new Word("at", Word.Parts.Conf, "で"),
			new Word("in", Word.Parts.Conf, "の中で"),
			new Word("into", Word.Parts.Conf, "の中に"),
			new Word("on", Word.Parts.Conf, "の上で"),
			new Word("to", Word.Parts.Conf, "にむかって"),
			new Word("with", Word.Parts.Conf, "と一緒に"),
			new Word("at", Word.Parts.Conf, "で"),
			new Word("a", Word.Parts.Article, "ある"),
			new Word("the", Word.Parts.Article, "その") };
	public void init(){
		//辞書を配列から作成する
		dict = new Hashtable<String, Word>();
		for(int i=0; i < startup.length; i++){
			dict.put(startup[i].title, startup[i]);
		}
//		dict2 = new Hashtable<String, String>();
//		for(int i=0; i < startup.length; i++){
//			dict2.put(startup[i].title, startup[i].part);
//		}
		
		//画面の配置
		english = new TextField(30);
		english.setFont(new Font("Serif", Font.PLAIN, 36));
		english.addActionListener(this);
		add(english);
		japanese = new TextField(30);
		japanese.setFont(new Font("Serif", Font.PLAIN, 36));
		add(japanese);
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		String source = english.getText();
		source = source.trim(); //余分な前後の空白を削除
		String[] words = source.split(" ");
		String result = "";
		String verbresult = "";  // 動詞を最後に持ってくるため
		String prepresult=""; //前置詞を後ろに持ってくるため
		for (String word: words){
			Word hit = dict.get(word);
			if(hit == null){
				result += word;
			}else{
				if(hit.part.equals("Verb")){
					verbresult = hit.wayaku;
				}else if(hit.part.equals("Prep")){
					prepresult += hit.wayaku;
				}else{
				result += hit.part;
				}
			}
		}
		//最後に動詞の訳を加えて表示する
		japanese.setText(result + prepresult + verbresult);
	}

}