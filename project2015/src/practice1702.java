import java.applet.*;
import java.awt.*;
import java.awt.event.*;

@SuppressWarnings("serial")
public class practice1702 extends Applet implements KeyListener{

	int code; //keyPressedで入力されたキーコード
	char c; //keyTypedで入力された文字
	
	public void init(){ //初期化
		addKeyListener(this);
	}
	
	public void paint(Graphics g){
		g.setFont(new Font("Serif", Font.PLAIN, 48));
		g.drawString("KeyCode:"+code, 50, 100);
		g.drawString("KeyChar:"+c, 50, 160);
	}
	
	public void keyTyped(KeyEvent e) {
		c = e.getKeyChar();
		repaint();
	}

	public void keyPressed(KeyEvent e) {
		code = e.getKeyCode();
		repaint();
	}
	
	public void keyReleased(KeyEvent e) {
		code = 0;
		repaint();
	}

}
