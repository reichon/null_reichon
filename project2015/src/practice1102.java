import java.applet.*;
import java.awt.*;

@SuppressWarnings("serial")
public class practice1102 extends Applet{
	
	Graphics g;
	enum Dir {North, South, East, West}; //方向を指定する
	
	public void paint(Graphics g){
		this.g = g;
		int centerx = getWidth() /2;
		int centery = getHeight() /2;
		int size = centerx * 8 /5;
		drawSquareKoch(centerx, centery * 3/2, size, Dir.North, 8);
	}
	
	void drawSquareKoch(double cx, double cy, double size, Dir dir, int level){
		if(level == 1){
			double half = size /2;
			if(dir == Dir.North || dir == Dir.South){
				g.drawLine((int)(cx-half), (int)cy, (int)(cx+half), (int)cy);
			}else{
				g.drawLine((int)cx, (int)(cy-half), (int)cx, (int)(cy+half));
			}
		}else{
			double third = size /3;
			double sixth = size /6;
			if(dir == Dir.North){
				drawSquareKoch(cx - third, cy, third, Dir.North, level-1);
				drawSquareKoch(cx - sixth, cy-sixth, third, Dir.West, level-1);
				drawSquareKoch(cx, cy-third, third, Dir.North, level-1);
				drawSquareKoch(cx + sixth, cy-sixth, third, Dir.East, level-1);
				drawSquareKoch(cx + third, cy, third, Dir.North, level-1);
			}else if(dir == Dir.South){
				drawSquareKoch(cx - third, cy, third, Dir.South, level-1);
				drawSquareKoch(cx - sixth, cy+sixth, third, Dir.West, level-1);
				drawSquareKoch(cx, cy+third, third, Dir.South, level-1);
				drawSquareKoch(cx + sixth, cy+sixth, third, Dir.East, level-1);
				drawSquareKoch(cx + third, cy, third, Dir.South, level-1);
			}else if(dir == Dir.East){
				drawSquareKoch(cx, cy-third, third, Dir.East, level-1);
				drawSquareKoch(cx+sixth, cy-sixth, third, Dir.North, level-1);
				drawSquareKoch(cx+third, cy, third, Dir.East, level-1);
				drawSquareKoch(cx+sixth, cy+sixth, third, Dir.South, level-1);
				drawSquareKoch(cx, cy+third, third, Dir.East, level-1);
			}else if(dir == Dir.West){
				drawSquareKoch(cx, cy-third, third, Dir.West, level-1);
				drawSquareKoch(cx-sixth, cy-sixth, third, Dir.North, level-1);
				drawSquareKoch(cx-third, cy, third, Dir.West, level-1);
				drawSquareKoch(cx-sixth, cy+sixth, third, Dir.South, level-1);
				drawSquareKoch(cx, cy+third, third, Dir.West, level-1);
			}
		}
	}
	
}
