import java.awt.*;
import java.applet.*;
import java.awt.event.*;

@SuppressWarnings("serial")
public class practice1704 extends Applet implements MouseListener{

	int px = 100, py = 100;
	public void init(){
		setBackground(Color.blue);
		addMouseListener(this);
	}
	
	Color pcolor = Color.yellow;
	
	public void paint(Graphics g){
		g.setColor(pcolor);
		g.fillArc(px-50, py-50, 100, 100, 45, 270);
	}

	Color colors[] = {Color.pink, Color.yellow, Color.red, Color.green};
	
	public void mouseClicked(MouseEvent e) {
		int count = e.getClickCount();
		pcolor = colors[count % colors.length];
	}

	public void mousePressed(MouseEvent e) {
		px = e.getX();
		py = e.getY();
		repaint();
	}

	public void mouseReleased(MouseEvent e) {}

	public void mouseEntered(MouseEvent e) {}

	public void mouseExited(MouseEvent e) {}

}
