import java.applet.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;

@SuppressWarnings("serial")
public class practice1801 extends Applet{

	public void paint(Graphics g){
		Graphics2D g2 = (Graphics2D)g;
		Rectangle2D.Double rect = new Rectangle2D.Double(10, 10, 100, 100);
		g.setColor(Color.yellow);
		g2.fill(rect);
		g.setColor(Color.red);
		g2.draw(rect);
		Arc2D.Double packman = 
				new Arc2D.Double(150, 10, 100, 100, 45, 270, Arc2D.PIE);
		g2.fill(packman);
		
		Arc2D.Double daiei = 
				new Arc2D.Double(290, 10, 100, 100, 270, 270, Arc2D.CHORD);
		g2.setColor(Color.orange);
		g2.fill(daiei);
		
		Arc2D.Double crescent =
				new Arc2D.Double(430, 10, 100, 100, 45, 270, Arc2D.OPEN);
		g2.setColor(Color.magenta);
		g2.draw(crescent);
		
		GeneralPath eiffel = createEiffel();
		AffineTransform at = new AffineTransform();
		at.translate(300, 300);
		eiffel.transform(at);
		for(int n = 1; n <= 6; n++){
			at = new AffineTransform();
			at.rotate(Math.toRadians(60), 300, 300);
			eiffel.transform(at);
			g2.draw(eiffel);
		}
	}
	
	GeneralPath createEiffel( ) {
		GeneralPath	eiffel = new GeneralPath( );
		eiffel.moveTo( -10, 0 );
		eiffel.lineTo( 10, 0 );
		eiffel.quadTo( 20, 100, 50, 150 );
		eiffel.lineTo( 20, 150 );
		eiffel.curveTo( 20, 120, -20, 120, -20, 150 );
		eiffel.lineTo( -50, 150 );
		eiffel.quadTo( -20, 100, -10, 0 );
		return eiffel;
	}
}
