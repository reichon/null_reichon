import java.applet.*;
import java.util.*;
import java.awt.*;
import java.awt.event.*;

@SuppressWarnings("serial")
public class practice1506 extends Frame{

	public static void main(String[] args) {
		new practice1506();
	}
	
	Panel textp, viewp;
	TextArea source;
	Button draw;
	
	//コンストラクタ
	public practice1506(){
		super("PostScript View");
		setSize(800, 400);
		setLayout(new GridLayout(1,2)); //1行2列
		//左側のパネル
		textp = new Panel();
		source = new TextArea(20, 15); //20行15文字
		textp.add(source);
		draw = new Button("描画");
		textp.add(draw);
		add(textp);
		
		//右側のパネル
		viewp = new Panel();
		add(viewp);
		
		setVisible(true);
	}
	
	public void paint(Graphics g){
		Graphics pg = viewp.getGraphics();
		pg.setColor(Color.yellow);
		pg.fillArc(50, 50, 200, 200, 45, 270);
	}

}
