import sfc.util.*;

public class practice1302 {

	public static void main(String[] args) {
		String line = Terminal.readLine("一行入力してください : ");
		System.out.println("最初の5文字" + line.substring(0,5));
		System.out.println("最後の5文字" + line.substring(line.length()-5));
		int the = line.indexOf("the");
		System.out.println("最初のtheが始まる位置 : " + the);
		int the2nd = line.indexOf("the", the+1);
		if(the >= 0 && the2nd >= 0){
			System.out.println("theに囲まれた文字列 : " + 
				line.substring(the + "the".length(), the2nd));
		}
	}

}
