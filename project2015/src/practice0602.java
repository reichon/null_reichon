
//ピタゴラス数の組合合わせを求める
//3 ** 2 + 4 ** 2 == 5 ** 2

public class practice0602 {

	public static void main(String[] args) {
		int up = 100; //上限
		int count = 0;
		for(int n=1; n<=up; n++){
			for(int m=n; m<=up; m++){
				for(int k=m; k<=up; k++){
					if(n*n + m*m == k*k){
						System.out.printf("(%d,%d,%d)\n", n, m, k);
					}
					count++;
				}
			}
		}
		System.out.println(count);
	}

}
