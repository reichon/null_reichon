import java.applet.*;
import java.awt.*;
import java.awt.event.*;

@SuppressWarnings("serial")
public class practice1705 extends Applet{
	
	int px = 100, py = 100;
	
	public void init(){
		this.addKeyListener(new MyKeyAdapter());
		this.addMouseListener(new MyMouseAdapter());
		setBackground(Color.blue);
	}
	
	public void paint(Graphics g){
		g.setColor(Color.yellow);
		g.fillArc(px-50, py-50, 100, 100, 45, 270);
	}
	
	class MyKeyAdapter extends KeyAdapter{
		@Override
		public void keyPressed(KeyEvent ke){
			if(ke.getKeyCode() == 37){px -= 10;}
			if(ke.getKeyCode() == 39){px += 10;}
			if(ke.getKeyCode() == 38){py -= 10;}
			if(ke.getKeyCode() == 40){py += 10;}
			repaint();
		}
	}
	
	class MyMouseAdapter extends MouseAdapter{
		@Override
		public void mousePressed(MouseEvent me){
			px = me.getX();
			py = me.getY();
			repaint();
		}
	}

}
