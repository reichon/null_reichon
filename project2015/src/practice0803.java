import  java.applet.*;
import  java.awt.*;

@SuppressWarnings("serial")
public class practice0803 extends Applet {

	public void paint(Graphics g) {
		int  centerx = getWidth() / 2;  // 中心のx座標
		int  centery = getHeight() / 2;  // 中心のy座標
		//座標系を描く
		g.setColor(Color.blue);
		g.drawLine(0, centery, getWidth(), centery);//水平線
		g.drawLine(centerx, 0, centerx, getHeight());//垂直線
		
		//線形螺旋(アルキメデスの螺旋)を描く
		g.setColor(Color.red);
		int  n = 10000;  // 何本の折れ線で曲線を近似するか
		int rot = 20;//10回転させる
		double  t = 0;  // 媒介変数
		double  delta = 2 * Math.PI * rot / n;  // 媒介変数の１回の差分
		double  radius = (centerx < centery) ? centerx *0.8 : centery *0.8;//最大半径
		double c = Math.log(radius) / (2 * Math.PI * rot);//係数
		
		double r = Math.exp(c * t);//動径
		int  lastx = centerx + (int)( r * Math.cos( t ) );
		int  lasty = centery - (int)( r * Math.sin( t ) );
		for ( int i=1; i <= n; i++ ) {
			t += delta;  // 差分を足す
			r = Math.exp(c * t); //動径を毎回求める
			int  x = centerx + (int)( r * Math.cos( t ) );
			int  y = centery - (int)( r * Math.sin( t ) );
			g.drawLine( lastx, lasty, x, y );
			lastx = x;
			lasty = y;
		}
	}
}