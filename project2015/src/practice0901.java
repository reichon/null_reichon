import java.util.*;

public class practice0901 {

	public static void main(String[] args) {
		Calendar gmt = Calendar.getInstance();
		System.out.println(gmt.getTime());
		Calendar cal = new GregorianCalendar(TimeZone.getTimeZone("JST"));
		cal = new GregorianCalendar(TimeZone.getTimeZone("GMT+09:00"));
		System.out.println(cal.getTime());
		System.out.println(cal.get(Calendar.YEAR));
		System.out.println(cal.get(Calendar.MONTH)+1);
		System.out.println(cal.get(Calendar.DATE));
		System.out.println(cal.get(Calendar.DAY_OF_WEEK));
		System.out.println(cal.get(Calendar.HOUR));
		System.out.println(cal.get(Calendar.HOUR_OF_DAY));
		System.out.println(cal.get(Calendar.MINUTE));
		System.out.println(cal.get(Calendar.SECOND));
		System.out.println(cal.get(Calendar.MILLISECOND));
		
		Calendar theday = Calendar.getInstance();
		Calendar otherday = Calendar.getInstance();
		theday.clear();
		otherday.clear();
		
		theday.set(2015, 10, 1); //2015/11/1 00:00:00
		otherday.set(2015, 11, 31, 23, 0, 30); //2015/12/31 23:00:30
		System.out.println(theday.getTime());
		System.out.println(otherday.getTime());
		
		//時間差を求める
		long thetime = theday.getTimeInMillis();
		long othertime = otherday.getTimeInMillis();
		long diff = othertime - thetime; //時間差をミリ秒で求める
		int second = (int)(diff / 1000 % 60); //秒にして60で割ったあまり
		int minute = (int)(diff / 1000 / 60 % 60); //分にして60で割ったあまり
		int hour = (int)(diff / 1000 / 3600 % 24); //時間にして24で割ったあまり
		int days = (int)(diff / 1000 / 3600 / 24); //日数にする
		System.out.printf("経過時間は%d日%02d時間%02d分%02d秒\n",
				days, hour, minute, second);
	}

}
