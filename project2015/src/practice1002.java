import  java.applet.*;
import  java.awt.*;

// 多重に呼出しをするメソッドの定義例

@SuppressWarnings("serial")
public class practice1002 extends Applet {
	Graphics g; //描画領域を指す共通の変数
	Color c = Color.blue;
	
	public void paint(Graphics g) {
		this.g = g;
		drawHouseArea(getWidth()/2, getHeight()/2, 15, 5, 30);
	}
	
	/**
	 * drawHouseArea
	 * @param cx 
	 * @param cy
	 * @param numx 家の横の個数
	 * @param numy 家の縦の個数
	 * @param size 家の個々のサイズ
	 */
	void drawHouseArea( int cx, int cy, int numx, int numy, int size ) {
		int deltax = 255/numx;
		int deltay = 255/numy;
		for(int j = 1, y = cy - size * 7/6 * numy; j <= numy; j++, y += size*3){
			for ( int i=1, x = cx - size * numx; i <= numx; i++, x += size*2  ) {
				c = new Color(255 - deltax * i, 128, deltay * j);
				drawHouse( x, y, size );
			}
		}
	}
	
	/**
	 * drawHouse 家を描くメソッド
	 * @param cx センターのx座標
	 * @param cy センターのy座標
	 * @param size 家のサイズ
	 */
	void drawHouse(  int cx, int cy, int size ) {
		drawTriangle( cx, cy-size , size ); //屋根
		g.drawRect( cx-size/2, cy-size/2, size, size ); //建物
		g.drawRect( cx-size/4, cy-size/4, size/2, size/2); //窓
	}
	
	/**
	 *正三角形を描くメソッド
	 *@param cx センターのx座標
	 *@param cy センターのy座標
	 *@param r 外接円の半径
	 */
	 void drawTriangle( int cx, int cy, int r ) {
		double  t=0;
		double  delta = 2 * Math.PI / 3;
		int lastx = (int)(r * Math.sin( t ) ) + cx;
		int lasty = -(int)( r * Math.cos( t ) ) + cy;
		g.setColor( c );
		for ( int n=1; n<=3; n++ ) {
			t += delta;
			int x = (int)(r * Math.sin( t ) ) + cx;
			int y = -(int)( r * Math.cos( t ) ) + cy;
			if(Math.abs(lasty-y) < 2){
				y = lasty;
			}
			g.drawLine( lastx, lasty, x, y );
			lastx = x;
			lasty = y;
		}
	}
}