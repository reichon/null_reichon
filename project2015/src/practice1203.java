
public class practice1203 {
	static String journeys[][] = {
		{"田原", "野村", "混同"},
		{"東山", "錦織", "植草"},
		{"中居", "木村", "草彅", "香取", "稲垣"},
		{"大野", "松本", "二宮", "櫻井", "相葉"},
		{"堂本", "堂本"},
		{"滝沢", "今井"},
		{"横山", "村上", "錦通", "内"}
	};
	static String groupnames[] = {"たのきんトリオ", "少年隊", "SMAP",
		"嵐", "Kinki Kids", "タッキー&翼", "関ジャニ∞"
	};
	
	public static void main(String[] arg){
		for(int i = 0; i < journeys.length; i++){
			System.out.print(groupnames[i] + ": ");
			for(int j = 0; j < journeys[i].length; j++){
				System.out.print(journeys[i][j]);
				if(j < journeys[i].length-1){
					System.out.print(", ");
				}
			}
			System.out.println();
		}
	}
}
