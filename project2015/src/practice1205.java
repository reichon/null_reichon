
//3次の正方行列(行数3,列数3)の演算
public class practice1205 {
	
	public static void main(String[] arg){
		int mata[][] = {{1,2,3},{4,5,6},{7,8,9}};
		int matb[][] = {{4,3,7},{1,4,9},{5,9,7}};
		printMatrix(mata);
		printMatrix(matb);
		int matc[][] = addMatrix(mata, matb);
		printMatrix(matc);
		int matm[][] = multiplyMatrix(mata, matb);
		printMatrix(matm);
		int det = determinant(mata);
		System.out.println(det);
	}
	
	static void printMatrix(int mat[][]){
		for(int i=0; i < mat.length; i++){
			for(int j = 0; j < mat[i].length; j++){
				System.out.printf("%2d ", mat[i][j]);
			}
			System.out.println();
		}
		System.out.println();
	}
	
	static int [][] addMatrix(int a[][], int b[][]){
		int [][] result = new int[a.length][a[0].length];
		for(int i=0; i < a.length; i++){
			for(int j = 0; j < a[i].length; j++){
				result[i][j] = a[i][j] + b[i][j];
			}
		}
		return result;
	}
	
	
	static int [][] multiplyMatrix(int a[][], int b[][]){
		int [][] result = new int[a.length][a[0].length];
		for(int i=0; i < a.length; i++){
			for(int j = 0; j < a[i].length; j++){
				int sum = 0;
				for(int k = 0; k < a.length; k++){
					sum += a[i][k] * b[k][j];
				}
				result[i][j] = sum;
			}
		}
		return result;
	}
	
	static int determinant(int mat[][]){
		int sum = mat[0][0] * mat[1][1] * mat[2][2];
		sum += mat[0][1] * mat[1][2] * mat[2][0];
		sum += mat[0][2] * mat[1][0] * mat[2][1];
		sum -= mat[0][2] * mat[1][1] * mat[2][0];
		sum -= mat[0][1] * mat[1][0] * mat[2][2];
		sum -= mat[0][0] * mat[1][2] * mat[2][1];
		return sum;
	}
}
