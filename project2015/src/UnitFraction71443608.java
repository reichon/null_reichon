import  java.util.*;

public class UnitFraction71443608 {

	// クラスメソッドを使う関係で、scannerもクラス変数にしています
	// staticが付くとクラス変数です
	static Scanner scanner;
	
	public static void main(String[] arg) {
		int bunshi = inputValue( "分子の数を入力：" );
		int bumbo = inputValue( "分母の数を入力：" );
		convertUnit( bunshi, bumbo );
	}
	
	// 直接呼び出せるようにクラスメソッドにしています
	// staticが付くとクラスメソッドです
	static int inputValue( String message ) {
		System.out.print( message );
		scanner = new Scanner( System.in );
		return   scanner.nextInt( );
	}

	/**
	 *  単位分数に直して表示するクラスメソッドです
	 *  これを再帰の形で書き直して下さい
	 * @param x 分子
	 * @param y 分母
	 */
	static void convertUnit( long x, long y ) {
		// 単位分数の分母を表示します
		long  unit = (long)(Math.ceil( (double)y / x ) );
		System.out.print( "1/" +  unit );
		// 次の分子分母を求めます
		long  bunshi = x - (y  % x );
		long  bumbo = y * (long)(Math.ceil( (double)y / x ) );
		while ( true ) {
			// 単位分数の分母を表示します
			unit = (long)(Math.ceil( (double)bumbo / bunshi ) );
			System.out.print( "   +  1/" + unit );
			// 分子分母を約分します
			long gcd = euclid( bunshi, bumbo );
			bunshi = bunshi / gcd;
			bumbo = bumbo / gcd;
			// 分子が１だったら終了です
			if ( bunshi == 1L ) { break; }
			// 次の分子分母を求めます
			x = bunshi - (bumbo  % bunshi);
			y = bumbo * (long)(Math.ceil( (double)bumbo / bunshi ) );
			// お互いに古い値を参照するので、一旦別の変数に求めておいてから
			// 代入してあげます
			bunshi = x;
			bumbo = y;
		}
	}
	
	/**
	 * ２つの数の最大公約数を求めるクラスメソッド
	 * @param n
	 * @param m
	 * @return 最大公約数
	 */
	static long euclid( long n, long m ) {
		long max = Math.max( n, m );
		long min = Math.min( n, m );
		return ( max % min == 0 ) ? min: euclid( min, max % min );
	}
}