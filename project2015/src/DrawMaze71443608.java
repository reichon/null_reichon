import java.applet.*;
import java.awt.*;

@SuppressWarnings("serial")
public class DrawMaze71443608 extends Applet {
	public void paint(Graphics g){
		int yoko = 53;
		int tate = 29;
		int maze[][] = new int[yoko][tate];
		
		//外壁を作る
		for(int i=0; i<maze.length; i++){
			maze[i][0] = 1;
			maze[i][tate-1] = 1;
		}
		for(int j=0; j<maze[0].length; j++){
			maze[0][j] = 1;
			maze[yoko-1][j] = 1;
		}
		
		//基準点を作る
		for(int i=2; i<maze.length-2;i+=2){
			for(int j=2; j<maze[i].length-2; j+=2){
				maze[i][j] = 1;
			}
		}
		
		
		//1列目の基準点から壁を伸ばす
		for(int j=2; j<maze[2].length-2; j+=2){
			int dir = (int)(Math.random()*4);
			if(dir == 0){maze[2][j-1] = 1;} //上
			if(dir == 1){maze[2][j+1] = 1;} //下
			if(dir == 2){maze[3][j] = 1;} //右
			if(dir == 3){maze[1][j] = 1;} //左
		}
		
		for(int m=2; m<52; m+=2){
			for(int j=2; j<maze[m].length-2; j+=2){
				int dir = (int)(Math.random()*3);
				if(dir == 0){maze[m][j-1] = 1;} //上
				if(dir == 1){maze[m][j+1] = 1;} //下
				if(dir == 2){maze[m+1][j] = 1;} //右
			}
		}
		
		//迷路を描画する
		g.setColor(Color.blue);
		for(int i=0; i<maze.length; i++){
			for(int j=0; j<maze[i].length; j++){
				if(maze[i][j] == 1){
					g.fillRect(i*20, j*20, 20, 20);
				}
			}
		}
	}
}
