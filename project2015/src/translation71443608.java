import java.util.*;
import java.applet.*;
import java.awt.*;
import java.awt.event.*;

@SuppressWarnings("serial")
public class translation71443608 extends Applet implements ActionListener{

	// 足りない単語は補充して下さい
	static String startup [][] = { 
		{"cat", "noun:猫"}, {"dog", "noun:犬"},
		{"ball", "noun:ボール"}, {"box", "noun:箱"},
		{"chair", "noun:椅子"}, {"floor", "noun:床"},
		{"garden", "noun:庭"}, {"rabbit", "noun:うさぎ"},
		{"room", "noun:部屋"}, {"sofa", "noun:ソファ"},
		{"jumps", "verb:ジャンプする"}, {"runs", "verb:走る"},
		{"gets", "verb:とる"}, {"looks", "verb:見る"},
		{"moves", "verb:動く"}, {"plays", "verb:遊ぶ"},
		{"sleeps", "verb:寝る"}, {"walks", "verb:歩く"},
		{"is", "verb:は"}, {"big", "adj:大きい"},
		{"black", "adj:黒い"}, {"blue", "adj:青い"},
		{"cold", "adj:冷たい"}, {"green", "adj:緑の"},
		{"heavy", "adj:重い"}, {"hot", "adj:ア↑ツゥイ↓"},
		{"large", "adj:広い"}, {"light", "adj:軽い"},
		{"small", "adj:小さい"}, {"orange", "adj:オレンジ色の"},
		{"pink", "adj:ピンク色の"}, {"red", "adj:赤い"},
		{"yellow", "adj:黄色い"}, {"white",  "adj:白い"},
		{"and", "prep:そして"}, {"at", "prep:で"},
		{"in", "prep:の中で"}, {"into", "prep:の中に"},
		{"on", "prep:の上で"}, {"to", "prep:にむかって"},
		{"with", "prep:と一緒に"}, {"at", "prep:で"},
		{"a", "article:ある"}, {"the", "article:その"} };
	
	TextField english, japanese;
	Hashtable<String,String>dict;
	
	public void init(){
		// startupの配列から、ハッシュテーブルを作る
		dict = new Hashtable<String,String>();
		for (int i=0; i < startup.length; i++){
			dict.put(startup[i][0], startup[i][1]);
		}
		
		//画面の配置
		english = new TextField(40);
		english.setFont(new Font("Serif", Font.PLAIN, 24));
		english.addActionListener(this);
		add(english);
		japanese = new TextField(60);
		japanese.setFont(new Font("Serif", Font.PLAIN, 24));
		add(japanese);
	}

	@Override
	public void actionPerformed(ActionEvent e){
		String source = english.getText();
		source = source.trim(); //余分な前後の空白を削除
		String words[] = source.split(" ");
		String result ="";
		String verbresult = ""; // 動詞を最後に持ってくるため
		String prepresult = ""; // 前置詞を後ろに持ってくるため
		for (int i=0; i < words.length; i++){
			//該当する英単語があるかどうか
			String trans = dict.get(words[i]);
			//ない場合はそのまま英単語を使う
			if(trans == null){
				result += words[i];
			}else{
				String wayaku = trans.substring(trans.indexOf(":")+1);
				String hinshi = trans.substring(0, trans.indexOf(":"));
				//もし品詞が動詞だった場合
				if(hinshi.equals("verb")){
					result += "は";
					verbresult = wayaku;
				}else if(hinshi.equals("prep")){
					result += prepresult;
					prepresult = wayaku;
				}else{
					result += wayaku;
				}
			}
		}
		//最後に動詞の訳を加えて表示する
		japanese.setText(result + prepresult + verbresult);
		
	}
}