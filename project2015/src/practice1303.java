import sfc.util.*;

public class practice1303 {

	public static void main(String[] args) {
		String source = "This is the sample message for you at the class.";
		System.out.println("置換前のテキスト: " + source);
		String find = Terminal.inputString("検索する文字列: ");
		String change = Terminal.inputString("置換する文字列: ");
		
		StringBuffer sb = new StringBuffer(source);
		//1つだけ直すカタチ
//		int findi = source.indexOf(find);
//		if(findi >= 0){
//			sb.replace(findi, findi+find.length(), change);
//		}
		
		//全て直すカタチ
		int findi = source.indexOf(find);
		while(findi >= 0){
			sb.replace(findi,  findi+find.length(), change);
			findi = sb.indexOf(find, findi+change.length());
		}
		System.out.println("置換後のテキスト: " + new String(sb));
	}

}
