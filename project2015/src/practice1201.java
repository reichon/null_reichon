
public class practice1201 {

	public static void main(String[] args) {
		practice1201 self = new practice1201();
		int a[] = {8, 9, 6, 0, 1, 3, 2, 7, 4, 5};
		self.sortArray(a);
		for(int i=0; i<a.length; i++){
			System.out.print(a[i] + " ");
			
		}
		System.out.println();
	}
	
	void sortArray(int[] a){
		for(int i=0; i<a.length-1; i++){
			//miniに一番小さい値が入っている要素のインデックスを
			int mini = i;
			for(int j=i+1; j<a.length; j++){
				if(a[mini] > a[j]){
					mini = j;
				}
			}
			//a[i]とa[mini]を入れ替える
			int temp = a[i];
			a[i] = a[mini];
			a[mini] = temp;
		}
	}
	
	int maxArray(int[] a, int start){
		//一番最後に来たら
		if(start >= a.length-1){
			return a[a.length-1];
		}
		//それ以外は、後ろの最大の要素の値と現在の値を比べる
		else{
			int max = maxArray(a, start+1);
			return(a[start] > max) ? a[start] : max;
		}
	}

}
