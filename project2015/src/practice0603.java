import sfc.util.*;

public class practice0603 {

	public static void main(String[] args) {
		int value = Terminal.inputInteger("素因数分解する数を入力：");
		int decomposed = value;
		while(decomposed > 1){
			//素数の数をprimeに求める
			for(int prime = 2; prime <= value; prime++){
				int count = 0;
				//primeが素数かどうか確かめる
				for(int n = 2; n <= prime; n++){
					if(prime % value == 0){count++;}
				}
				//primeが素数だったら割ってみる
				if(count == 1){
					if(decomposed % prime == 0){
						System.out.print(prime + " ");
						decomposed = decomposed / prime;
					}
				}
			}
		}
		System.out.println();
	}

}
