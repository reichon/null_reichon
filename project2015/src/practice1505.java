import java.awt.*;
import java.awt.event.*;

@SuppressWarnings("serial")
public class practice1505 extends Frame implements ActionListener {

	public static void main(String[] args) {
		new practice1505();

	}
	
	Button b;
	int px = 100;
	
	//コンストラクタ
	public practice1505(){
		super("packman window");
		setBackground(Color.blue);
		setSize(500, 200);
		setLayout(new FlowLayout());
		b = new Button("go east");
		b.addActionListener(this);
		add(b);
		setVisible(true);
	}
	public void paint(Graphics g){
		g.setColor(Color.yellow);
		g.fillArc(px,  50, 100, 100, 45, 270);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		px = (px + 10) % getWidth();
		repaint();
	}

}
