import sfc.util.*;

public class practice1301 {

	public static void main(String[] args) {
		String text = Dialogue.input("input text : ", "シブがき隊");
		System.out.println("文字数 : " + text.length()); //文字列はlength()
		if(text == "少年隊" || text.equals("少年隊")){
			System.out.println(text + "には東山くんがいます");
		}
		if(text.endsWith("隊")){
			System.out.println(text + "はバク転ができます");
		}
		if(text.startsWith("SM")){
			System.out.println(text + "は歌唱力がありません");
		}
		if(text.compareTo("あ") < 0){
			System.out.println("英語名のグループです");
		}else if(text.compareTo("一") > 0){ //漢字の1
			System.out.println("漢字名のグループです");
		}else{
			System.out.println("かな名のグループです");
		}
		System.exit(0); //dialogueを使った場合
	}

}
