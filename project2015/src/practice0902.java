import java.util.*;
import java.applet.*;
import java.awt.*;

public class practice0902 extends Applet {

	public void paint(Graphics g) {
		Calendar cal = Calendar.getInstance();
		g.setFont(new Font("Serif", Font.ITALIC, 56));
		g.drawString(cal.getTime().toString(), 10, 100);
		String s = String.format("%02d時%02d分%02d秒",
				cal.get(Calendar.HOUR), cal.get(Calendar.MINUTE),
				cal.get(Calendar.SECOND));
		g.drawString(s, 10, 180);
		//Calendarのサブクラスが、GregorianCalendarになっている
		GregorianCalendar gc = (GregorianCalendar)(Calendar.getInstance());
		Calendar c = GregorianCalendar.getInstance();
		System.out.println(gc + "\n" + c);
	}

}
