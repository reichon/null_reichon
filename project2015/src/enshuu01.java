import  java.util.*;

public class enshuu01 {

	// クラスメソッドを使う関係で、scannerもクラス変数にしています
	// staticが付くとクラス変数です
	static Scanner scanner;
	
	public static void main(String[] arg) {
		int bunshi = inputValue( "分子の数を入力：" );
		int bumbo = inputValue( "分母の数を入力：" );
		convertUnit( bunshi, bumbo );
	}
	
	// 直接呼び出せるようにクラスメソッドにしています
	// staticが付くとクラスメソッドです
	static int inputValue( String message ) {
		System.out.print( message );
		scanner = new Scanner( System.in );
		return   scanner.nextInt( );
	}

	/**
	 *  単位分数に直して表示するクラスメソッドです
	 *  これを再帰の形で書き直して下さい
	 * @param x 分子
	 * @param y 分母
	 */
	static void convertUnit( long x, long y ) {
		// 単位分数の分母を表示します
		long  unit = (long)(Math.ceil( (double)y / x ) );
		System.out.print( "1/" +  unit );
		// 次の分子分母を求めます
		long  bunshi = x - (y  % x );
		long  bumbo = y * (long)(Math.ceil( (double)y / x ) );
		fraction(bunshi, bumbo);
	}
	
	static long fraction( long a, long b ){
		long unit = (long)(Math.ceil((double)b / a));
		System.out.print("　+　1/" + unit);
		long gcd = euclid(b, a);
		a = a /gcd;
		b = b / gcd;
		long f = a - (b % a);
		long g = b * (long)(Math.ceil((double)b / a));
		
		return(a == 1L) ? b: fraction(f, g);
	}
	
	/**
	 * ２つの数の最大公約数を求めるクラスメソッド
	 * @param n
	 * @param m
	 * @return 最大公約数
	 */
	static long euclid( long n, long m ) {
		long max = Math.max( n, m );
		long min = Math.min( n, m );
		return ( max % min == 0 ) ? min: euclid( min, max % min );
	}
}