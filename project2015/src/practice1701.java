import java.applet.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

//3次の正方行列を表すクラス
class Matrix3{
	//データを持つインスタンス変数
	double value[][] = new double[3][3];
	
	//コンストラクタ
	public Matrix3(){}
	public Matrix3(Matrix3 original){
		for(int i=0; i<value.length; i++){
			for(int j=0; j<value[i].length; j++){
				value[i][j] = original.value[i][j];
			}
		}
	}
	public Matrix3(double mat[][]){
		for(int i=0; i<value.length; i++){
			for(int j=0; j<value[i].length; j++){
				value[i][j] = mat[i][j];
			}
		}
	}
	public Matrix3 add(Matrix3 aite){
		Matrix3 result = new Matrix3();
		for(int i=0; i<value.length; i++){
			for(int j=0; j<value[i].length; j++){
				result.value[i][j] = value[i][j] + aite.value[i][j];
			}
		}
		return result;
	}
	public Matrix3 multiply(Matrix3 aite){
		Matrix3 result = new Matrix3();
		for(int i=0; i<value.length; i++){
			for(int j=0; j<value[i].length; j++){
				double sum = 0.0;
				for(int k=0; k<value.length; k++){
					sum += value[i][k] * aite.value[k][j];
				}
				result.value[i][j] = sum;
			}
		}
		return result;
	}
}

@SuppressWarnings("serial")
public class practice1701 extends java.applet.Applet implements ActionListener{

	JTable tablea, tableb, tablec; //行列表示のためのテーブル
	JButton assign; //=のボタン
	JComboBox<String> operator; //演算子
	JPanel jp;
	
	public void init(){
		jp = new JPanel();
		jp.setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
		Font f = new Font("SansSerif", Font.PLAIN, 28);
		//テーブルAを作る
		tablea = new JTable(3,3);
		tablea.setFont(f);
		jp.add(tablea);
		//演算子の選択メニューを作る
		String ops[] = {"+", "×"};
		operator = new JComboBox(ops);
		jp.add(operator);
		//テーブルBを作る
		tableb = new JTable(3,3);
		tableb.setFont(f);
		jp.add(tableb);
		//計算の=ボタンを作る
		assign = new JButton("=");
		assign.addActionListener(this);
		jp.add(assign);
		//結果のテーブルCを作る
		tablec = new JTable(3,3);
		tablec.setFont(f);
		jp.add(tablec);
		add("Center", jp);
	}
	
	public void paint(Graphics g) {

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
	}

}
