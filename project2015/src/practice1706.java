import java.awt.*;
import java.applet.*;
import java.awt.event.*;
import java.util.*;

//線を表すクラス
class Line{int sx, sy, ex, ey;}

@SuppressWarnings("serial")
public class practice1706 extends Applet {
	
	int sx, sy, ex, ey; //現在描いている線
	ArrayList<Line> lines; //今まで描いた線
	
	public void init(){
		lines = new ArrayList<Line>();
		addMouseListener(new MyMouseAdapter());
		addMouseMotionListener(new MyMotionAdapter());
	}
	
	public void paint(Graphics g){
		g.setColor(Color.blue);
		for(Line line: lines){
			g.drawLine(line.sx, line.sy, line.ex, line.ey);
		}
		g.setColor(Color.red);
		g.drawLine(sx, sy, ex, ey);
	}
	
	class MyMouseAdapter extends MouseAdapter{
		@Override
		public void mousePressed(MouseEvent me){
			sx = ex = me.getX();
			sy = ey = me.getY();
			repaint();
		}
		@Override
		public void mouseReleased(MouseEvent me){
			Line line = new Line();
			line.sx = sx; line.sy = sy;
			line.ex = ex; line.ey = ey;
			lines.add(line);
		}
	}
	
	class MyMotionAdapter extends MouseMotionAdapter{
		@Override
		public void mouseDragged(MouseEvent me){
			ex = me.getX();
			ey = me.getY();
			repaint();
		}
	}
}
