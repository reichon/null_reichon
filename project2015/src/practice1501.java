import java.util.*;
import java.applet.*;
import java.awt.*;
import java.awt.event.*;

@SuppressWarnings("serial")
public class practice1501 extends Applet implements ActionListener {

	TextField inputf; //入力フィールド
	TextArea resultf; //出力フィールド
	
	public void init() {
		inputf = new TextField(30); //30文字くらいはいる
		inputf.addActionListener(this); //return/enterキーを押すとこのアプレットが対処
		add(inputf); //画面上にコンポーネントを配置
		resultf = new TextArea(20, 30); //20行で30文字
		add(resultf);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		String text = inputf.getText(); //入力されたテキストを持ってくる
		resultf.append(text + "\n");
		inputf.setText(""); //入力フィールドはクリアする
	}

}
