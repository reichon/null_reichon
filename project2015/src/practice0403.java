import sfc.util.*;


public class practice0403 {
	public static void main(String[] arg){
		int value = Dialogue.input("生の整数入力", 67);
		if(value < 0){value = - value;}
		if(value > 80){
			value = 100;
		}else{
			value = value + 20;
		}
		String rank;
		if(value >= 80){
			rank = "A";
		}else if(value >= 60){
			rank = "B";
		}else if(value >= 40){
			rank = "C";
		}else{
			rank = "D";
		}
		Dialogue.display(value + "点の成績では" + rank);
	}
}