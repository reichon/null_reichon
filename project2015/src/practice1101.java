import java.applet.*;
import java.awt.*;

@SuppressWarnings("serial")
public class practice1101 extends Applet{
	Graphics g;
	
	public void paint(Graphics g){
		this.g = g;
		int centerx = getWidth() /2;
		int centery = getHeight() /2;
		int radius = (centerx < centery) ? centerx * 4/5 : centery * 4/5;
		drawSierpinski(centerx, centery, radius, 7);
	}
	
	void drawSierpinski(double cx, double cy, double r, int level){
		//正三角形を描く
		double t = 0.0;
		int lastx = (int)(r * Math.sin(t) + cx);
		int lasty = (int)(-r * Math.cos(t) + cy);
		for(int n = 1; n <= 3; n++){
			t += Math.PI * 2/3.0;
			System.out.println(t);
			int x = (int)(r * Math.sin(t) + cx);
			int y = (int)(-r * Math.cos(t) + cy);
			g.drawLine((int)x, (int)y,  (int)lastx, (int)lasty);
			lastx = x;
			lasty = y;
		}
		
		//再帰でサブのギャスケットを描かせる
		 if( level > 1 ){
			   drawSierpinski(cx, cy-r/2, r/2, level-1);
			   drawSierpinski(cx-r*Math.sqrt(3)/4, cy+r/4, r/2, level-1);//左
			   drawSierpinski(cx+r*Math.sqrt(3)/4, cy+r/4, r/2, level-1);//右
		 }
	}
}
