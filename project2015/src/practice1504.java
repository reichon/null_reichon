class Bunsu{
	int bunshi;
	int bumbo;

	public Bunsu(){bunshi=0; bumbo=1;}
	public Bunsu(int bs, int bm){
		if(bm < 0){bunshi = -bs; bumbo = -bm;}
		else{bunshi = bs; bumbo = bm;}
	}

	//約分をするメソッド
	public void yakubun(){
		int yakusu = gcd((bunshi<0) ? -bunshi: bunshi, bumbo);
		bunshi = bunshi/yakusu;
		bumbo = bumbo/yakusu;
	}
	//符号を分子に持ってくるメソッド
	public void sign(){
		if(bumbo < 0) {
			bunshi = -bunshi; bumbo = -bumbo;
		}
	}
	
	//最大公約数を求めるメソッド
	int gcd(int n, int m){
		int max = Math.max(n, m);
		int min = Math.min(n, m);
		if(max % min == 0){return min;}
		else{return gcd(min, max % min);}
	}
	
	//文字列に変換するメソッド
	public String toString(){
		return bunshi + "/" + bumbo;
	}
	
	//足し算をするメソッド
	public Bunsu add(Bunsu aite){
		Bunsu result = new Bunsu();
		result.bumbo = this.bumbo * aite.bumbo;
		result.bunshi = this.bunshi * aite.bumbo + this.bumbo * aite.bunshi;
		result.sign();
		result.yakubun();
		return result;
	}
	
	//掛け算をするメソッド
	public Bunsu multiply(Bunsu aite){
		Bunsu result = new Bunsu();
		result.bumbo = this.bumbo * aite.bumbo;
		result.bunshi = this.bunshi * aite.bumbo;
		result.sign();
		result.yakubun();
		return result;
	}
}

public class practice1504{
	public static void main(String[] arg){
		Bunsu a, b, c, d;
		a = new Bunsu(12,15);
		b = new Bunsu(24,60);
		c = a.add(b);
		d = a.multiply(b);
		System.out.println(a);
		System.out.println(b);
		System.out.println(c);
		System.out.println(d);
	}
}