
public class practice0500 {
	public static void main(String[] arg){
		int x = 10, y = 20;
		if(x <= 10 && y <= 10){
			System.out.println();
		}
		if(x >= 10 && x <= 20){
			System.out.println();
		}
		if(x % 13 != 0){
			System.out.println();
		}
		if(x % 13 == 0 && x % 17 != 0){
			System.out.println();
		}
		if(x % 2 == 0 || y % 2 == 0){
			System.out.println();
		}
		if(x % 10 % 7 == 0){
			System.out.println();
		}
		if(12 % y == 0){
			System.out.println();
		}
		if(y % 10 + y / 10 % 10 == 10){
			System.out.println();
		}
	}
}
